import org.junit.Test;

import static org.junit.Assert.*;


public class CalculatorTest {
    @Test
    public void add() throws Exception {
        assertEquals(4,Calculator.add(2,2));
    }

    @Test
    public void sub() throws Exception {
        assertEquals(0,Calculator.sub(2,2));

    }

    @Test
    public void mul() throws Exception {
        assertEquals(4,Calculator.mul(2,2));

    }

    @Test
    public void div() throws Exception {
        assertEquals(1,Calculator.div(2,0));

    }

}