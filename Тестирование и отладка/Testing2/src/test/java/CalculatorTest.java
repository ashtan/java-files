import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * Created by anton on 22.01.18.
 */
public class CalculatorTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }
    @Test
    public void main() throws Exception {
        Calculator.main(new String[] {""});
        assertEquals("Hello!\n", outContent.toString());
    }

    @org.junit.Test
    public void add() throws Exception {
        assertEquals(4,Calculator.add(2,2));
    }

    @org.junit.Test
    public void sub() throws Exception {
        assertEquals(0,Calculator.sub(2,2));
    }

    @Ignore
    @org.junit.Test
    public void mul() throws Exception {
        assertEquals(4,Calculator.mul(2,2));
    }

    @org.junit.Test(expected = ArithmeticException.class)
    public void div() throws Exception {
        assertEquals(2,Calculator.div(2,0));
    }

}

