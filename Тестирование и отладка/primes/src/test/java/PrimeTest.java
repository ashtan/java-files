import java.util.ArrayList;

import static org.junit.Assert.*;

public class PrimeTest {

    @org.junit.Test
    public void testPrime() {
        assertEquals(true,Prime.testPrime(17));
    }

    @org.junit.Test
    public void testNonPrime() {
        assertEquals(false,Prime.testPrime(27));
    }
    @org.junit.Test
    public void nextPrime() {
            assertEquals(7,Prime.nextPrime(5));
    }

    @org.junit.Test
    public void nPrime() {
        assertEquals(3,Prime.nPrime(2,1));
    }
    @org.junit.Test(timeout=50)
    public void testArr() {
        ArrayList<Integer> arr=Prime.get2first();
        assertEquals(2,(int)arr.get(0));
        assertEquals(3,(int)arr.get(1));
    }
}