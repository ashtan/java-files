import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.runner.JUnitCore;
import primes.*;
 
public class TestPrime {
 
    @Test
    public void isPrime1() {
        assertEquals(true, Prime.isPrime(17));
    }
    @Test
    public void isPrime2() {
        assertEquals(false, Prime.isPrime(4));
    }
    @Test
    public void isPrime3() {
        assertEquals(2, Prime.nPrime(1));
    }
    @Test
    public void isPrime4() {
        assertEquals(3, Prime.nPrime(2));
    }         
 
    public static void main(String[] args) {
        JUnitCore core = new JUnitCore();
        core.run(TestPrime.class);
    }
}