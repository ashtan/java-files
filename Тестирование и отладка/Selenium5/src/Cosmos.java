import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class Cosmos {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        System.setProperty("webdriver.gecko.driver", "/Users/anton/Downloads/geckodriver");
        driver = new FirefoxDriver();
        baseUrl = "https://www.google.ru/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void testCosmos() throws Exception {
        driver.get("https://www.google.ru/");
        //driver.findElement(By.id("lst-ib")).click();
        //driver.findElement(By.id("lst-ib")).clear();
        //driver.findElement(By.id("lst-ib")).sendKeys("День космонавтики");
        //driver.findElement(By.id("lst-ib")).sendKeys(Keys.DOWN);
        //driver.findElement(By.id("lst-ib")).sendKeys(Keys.ENTER);
        //driver.findElement(By.linkText("День космонавтики — Википедия")).click();
        /*try {
            assertTrue(driver.findElement(By.cssSelector("BODY")).
                    getText().matches("^[\\s\\S]*1961[\\s\\S]*$"));
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }*/
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
