import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.runner.JUnitCore;
 
public class TestCalc {
 
    @Test
    public void getSumTest() {
        Calc c = new Calc();
        assertEquals(50, c.getSum(20, 30));
    }
 
    @Test
    public void getSubTest() {
        Calc c = new Calc();
        assertEquals(-10, c.getSub(20, 30));
    }
    @Test
    public void getMulTest() {
        Calc c = new Calc();
        assertEquals(600, c.getMul(20, 30));
    }
     @Test
    public void getDivTest() {
        Calc c = new Calc();
        assertEquals(0.66, c.getDiv(20, 30));
    }

    public static void main(String[] args) {
        JUnitCore core = new JUnitCore();
        core.run(TestCalc.class);
    }
}