import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.List;

public class MainClass {
    public static void main(String[] args) throws ParseException {
        Enviroment.initDriver();
        WebDriver webDriver = Enviroment.getWebDriver();
        try {
            webDriver.get("http://api.open-notify.org/astros.json");
            System.out.println(webDriver.getPageSource());
            WebElement elem = webDriver.findElement(By.id("json"));
            String json_str = elem.getText();
            JSONParser parser = new JSONParser();
            JSONObject obj = (JSONObject) parser.parse(json_str);
            JSONArray people = (JSONArray) obj.get("people");
            System.out.println("TASK 1");
            System.out.println("Count: " + people.size());
            for (int i = 0; i < people.size(); ++i) {
                JSONObject person = (JSONObject) people.get(i);
                System.out.println((i + 1) + ") " + person.get("craft") + " " + person.get("name"));
            }
            System.out.println();
        } catch (Exception e) {
            System.out.println("Error in TASK 1");
        }

        try {
            System.out.println("TASK 2");
            webDriver.get("https://www.anekdot.ru/");
            WebElement elem = webDriver.findElement(By.xpath("/html/body/div[2]/div[9]/form/input[1]"));
            elem.sendKeys("космонавт");
            elem = webDriver.findElement(By.xpath("/html/body/div[2]/div[9]/form/input[2]"));
            elem.click();
            List<WebElement> elems = webDriver.findElements(By.className("topicbox"));
            for (WebElement webElem: elems) {
                try {
                    WebElement joke = webElem.findElement(By.className("text"));
                    String str = joke.getText();
                    System.out.println(str);
                    System.out.println();
                } catch (Exception e) {}
            }
        } catch(Exception e) {
            System.out.println("Error in TASK 2");
        }

        try {
            System.out.println("TASK 3");
            webDriver.get("https://yandex.ru/pogoda/nizhny-novgorod/details#11");
            WebElement elem = webDriver.findElement(By.className("weather-table"));
            List<WebElement> elems = elem.findElements(By.className("weather-table__row"));
            for (WebElement webElem: elems) {
                WebElement part = webElem.findElement(By.className("weather-table__daypart"));
                System.out.print(part.getText() + ": ");
                WebElement temp = webElem.findElement(By.className("weather-table__temp"));
                System.out.print(temp.getText());
                //WebElement rain = webElem.findElement(By.className("weather-table__body-cell weather-table__body-cell_type_condition"));
                //System.out.print(rain.getText());
                System.out.println();

            }
            System.out.println("OK");
        }
        catch (Exception e) {
            System.out.println("Error in TASK 3");
        }
        Enviroment.shotDown();
    }
}
