import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Enviroment {
    private static WebDriver webDriver;

    public static WebDriver getWebDriver() {
        return webDriver;
    }

    public static void setWebDriver(WebDriver webDriver) {
        Enviroment.webDriver = webDriver;
    }

    public static void initDriver() {
        System.setProperty("webdriver.gecko.driver", "C:\\Frameworks\\geckodriver-v0.24.0-win64\\geckodriver.exe");
        webDriver=new FirefoxDriver();
    }

    public static void shotDown() {
        webDriver.quit();
    }
}
