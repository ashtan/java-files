import static org.junit.Assert.*;

public class PrimeTest {

    @org.junit.Test
    public void testPrime() {
        Sqrt q1=new Sqrt(2,0.001);
        assertEquals(1.456,q1.calc(),0.001);


    }
    @org.junit.Test
    public void testNonPrime() {
        assertEquals(false,Prime.testPrime(12));
    }

    @org.junit.Test(timeout=30)
    public void nextPrime() {
        assertEquals(104729,Prime.nextPrime(104723));
    }

    @org.junit.Test(timeout=30)
    public void nPrime() {
        assertEquals(104729,Prime.nPrime(2,9999));
    }
}