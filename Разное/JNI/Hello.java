public class Hello
{
	// native function
	public native void hello();
	public static void main(String[] args) {
       Hello h=new Hello();
       h.hello();
	}

	static {
		//System.out.println(System.getProperty("java.library.path"));
		//System.load("/Users/iMac/Research/Java/JNI/libhello.so");
		System.loadLibrary("hello");
	}
}