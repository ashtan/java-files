Краткая заметка на тему: как через JNI связать Java и С
========================================================

1. Пишем Java-код, например:

public class Hello
{
	// native function
	public native void hello();
	public static void main(String[] args) {
       Hello h=new Hello();
       h.hello();
	}

	static {
		System.loadLibrary("hello");
	}
}

2. Компилируем Java-программу:

javac Hello.java

3. Создаем заголовочный файл Hello.h:

javah -jni Hello

4. Создаем файл hello.c с определением функции hello():

#include <jni.h>
#include <stdio.h>
#include "Hello.h"

JNIEXPORT void JNICALL Java_Hello_hello(JNIEnv *e, jobject o)
{
	printf("Hello from C!");
}

Имя вызываемой функции берем из h-файла.

5. Компилируем С-файл и получаем библиотеку:

g++ -dynamiclib -o libhello.jnilib -I/opt/local/lib/gcc47/gcc/x86_64-apple-darwin13/4.7.4/include hello.c

Здесь в параметре -I указан путь к заголовочному файлу jni.h

6. Запускаем Java-программу:

java Hello

Источники:
http://mrjoelkemp.com/2012/01/getting-started-with-jni-and-c-on-osx-lion/
