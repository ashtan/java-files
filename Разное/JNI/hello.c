#include <jni.h>
#include <stdio.h>
#include "Hello.h"

JNIEXPORT void JNICALL Java_Hello_hello(JNIEnv *e, jobject o)
{
	printf("Hello from C!");
}