
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Reader;
import java.io.Writer;

class FileDemo
{
    public static void main(String[]args) throws Exception
    {
        Reader reader; 
        Writer writer; 
        reader = new FileReader(args[0]); 
        writer = new FileWriter(args[1]); 
        int c = 0;

        while ((c = reader.read()) >= 0) {
           writer.write(Character.toUpperCase((char) c)); 
       }
     reader.close();
     writer.close(); 
    }
}
