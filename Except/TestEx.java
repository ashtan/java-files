
import java.io.IOException;


public class TestEx
{
    public static int calc(int m,int n) throws IOException{
        if(m==5)
           throw new ArithmeticException("Бух!");
         else if(m==6)
           throw new IOException("Ошибка ввода/вывода");
        return m/n;
    }
    public static void main(String[] args) {
        int result=0;
        byte b=0;
       try {

          result=calc(16,10);
          throw new ArithmeticException("Very, very bad!");
       }
       catch(ArithmeticException ex) {
           System.out.println("Ахтунг! "+ex);
           throw ex;
       }
       catch(Exception ex) {
           System.out.println("Увага! "+ex);
       }
       finally {
           System.out.println("Finished! "+result);
       }
    }
}
