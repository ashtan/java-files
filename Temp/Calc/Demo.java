class Calc {
   public int add(int x,int y) {return x+y;}
   public int sub(int x,int y) {return x-y;}
   public int mul(int x,int y) {return x*y;}
   public int div(int x,int y) {return x/y;}
}

class ECalc extends Calc {
    public double log(double x) {return 0.0; }
    public int add(int x,int y) {return x*x+y*y;}
}

class FCalc extends Calc {
    public double proc(double x) {return 0.0; }
}

public class Demo {
    public static void main(String[] args) {
        Calc ec=new ECalc();
        System.out.println(ec.add(3,4));
    }
}




