import java.util.ArrayList;


interface Doorable {
    void open();
    void close();
}

abstract class Window {
    protected int x,y,width,height;
    abstract void show();
    abstract void hide();
}

class Button extends Window {
    void show() { System.out.println("Button show");}
    void hide() { System.out.println("Button hide");}
}
class Dialog extends Window {
    void show() { System.out.println("Dialog show");}
    void hide() { System.out.println("Dialog hide");}
}

public class Demo {
    public static void main(String[] args) {
        ArrayList<Window> form=new ArrayList<>();
        form.add(new Button());
        form.add(new Button());
        form.add(new Dialog());
        form.add(new Button());

        for(Window item: form)
            item.show();

    }
}