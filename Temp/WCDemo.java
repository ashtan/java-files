interface Box<T> { 
    public T get();
    public void put(T element); 
}
class MyBox<T> implements Box<T> { 
    T item;
    public void put(T element) { 
        item=element;
    }
    public T get() {
       return item; 
    }
}
public class WCDemo {
     public static void unbox(Box<?> box) { 
        System.out.println(box.get());
     }
     public static void main(String[] args) {
       MyBox<Object> mb=new MyBox<>(); 
       mb.put(12);
       unbox(mb);
     }
 }