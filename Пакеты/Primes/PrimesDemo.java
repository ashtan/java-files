import primes.Prime;

class PrimesDemo
{
	public static void main(String[] args)
	{
		int num=Integer.parseInt(args[0]);
		System.out.println(""+num+" is prime = "+Prime.isPrime(num));
	}
}