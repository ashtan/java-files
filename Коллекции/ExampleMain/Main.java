import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
 
/**
 * Демонстрация различных операций над списком
 */
public class Main {
 
    public static void main(String[] args) {
 
        List<String> list = new ArrayList<String>();
 
        list.add(new String("Василий"));
        list.add(new String("Павел"));
        list.add(new String("Андрей"));
        list.add(new String("Андрей"));
        list.add(new String("Петр"));
        list.add(new String("Анжелика"));
 
        printCollection("Оригинал", list);
 
        // Смешивание
        Collections.shuffle(list);
        printCollection("Смешивание", list);
 
        // Обратный порядок
        Collections.reverse(list);
        printCollection("Обратный порядок", list);
 
        // "Проворачивание" на определенное количество
        Collections.rotate(list, 2); // Число может быть отрицательным - тогда порядок будет обратный
        printCollection("Проворачивание", list);
 
        // Обмен элементов
        Collections.swap(list, 0, list.size()-1);
        printCollection("Обмен элементов", list);
 
        // Замена
        Collections.replaceAll(list, new String("Андрей"), new String("Алексей"));
        printCollection("Замена", list);
 
        // Копирование - здесь обязательно надо иметь нужные размеры
        List<String> list2 = new ArrayList<String>(list.size());
        // Поэтому заполняем список. Хоть чем-нибудь.
        for(String mc : list) {
            list2.add(null);
        }
        // Компируем из правого аргумента в левый
        Collections.copy(list2, list);
        printCollection("Копирование", list2);
 
        // Полная замена
        Collections.fill(list2, new String("Антон"));
        printCollection("Полная замена", list2);
 
    }
 
    private static void printCollection(String title, List<String> list) {
        System.out.println(title);
        for(String mc : list) {
            System.out.println("Item:" + mc);
        }
        System.out.println();
    }
 
}