import java.util.ArrayList;

class Box<T> 
{   
    private T t;           
    public void put(T t) 
	  {   
       this.t = t; 
    }
    public T get() 
	  { 
       return t;  
    }
    public <U> void inspect(U u)
    {
      System.out.println(
         "T: " + t.getClass().getName());
      System.out.println(
         "U: " + u.getClass().getName()); 
    }
}
public class Program
{
   public static void main(String[] args)
   {
      Box<String> sbox=new Box<String>();
      sbox.put("box");
      sbox.inspect(new ArrayList<Integer>());

   }
}