import java.util.*;

public class Example3
{
  public static void main(String[] args) {
  	  List<String> colors =
      Arrays.asList("Red", "Orange", "Blue", "White", "Black", "Yellow");
      List<String> colorLens = new ArrayList<String>();
      List<String> bColors = new ArrayList<String>();


      colors.stream()
            .map(color -> color.length())
            .forEach(color -> { colorLens.add(color.toString());
            	                System.out.print(color + " ");});

      colors.stream().filter(color -> color.startsWith("B"))
                     .forEach(color -> bColors.add(color.toString()));
      
     System.out.println(bColors);
  }
}