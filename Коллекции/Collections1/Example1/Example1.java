import java.util.*;

public class Example1
{
  public static void main(String[] args) {
     Integer[] arr=new Integer[] {1,2,3,4,5,6,7,8};
     List<Integer> list2=new ArrayList<>();
     list2 = Arrays.asList(arr);
     
     list2.set(1,9);
    // перебор коллекции
    for(Integer number: list2) 
      System.out.println(number);

  	  List<String> colors = new ArrayList<>();
      String[] cols=new String[] {"Red", "Orange", "Blue", "White", "Black", "Yellow"};
      colors=Arrays.asList(cols);

   System.out.println(colors.getClass().getName());
    //for(String c:cols)
    //  colors.add(c);

    //colors.addAll(Arrays.asList(cols));
    cols[0]="Violet";
    //colors.add("Grey");
    colors.set(0,"!!!");

    //for(String c:cols)
    //  System.out.println(c);

    // опасный цикл
  	//for(int i=0;i<colors.size();i++)
  	//	System.out.println(colors.get(i));

    // перебор коллекции
  	//for(String color: colors) 
  	//	System.out.println(color);

    // перебор с лямбда-функцией
  	//colors.forEach((String color) -> System.out.println(color));

    // перебор с лямбда-функцией
  	//colors.forEach(System.out::println);
    colors.forEach(Proc::fun);

  }
}

class Proc {
  public static void fun(String str) {
     System.out.println("I like "+str);
  }
}