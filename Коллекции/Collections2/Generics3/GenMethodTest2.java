class GenMethod
{
    public static <T> boolean present(T[] arr, T item) {
        for(int i=0;i<arr.length;i++)
            if(arr[i].equals(item))
                return true;
        return false;
    }
}
public class GenMethodTest2 {
    public static void main(String[] args) {
        Integer[] arr=new Integer[] {1,2,3,4};

        for(int i: arr) {
            System.out.println(i);
        }
        System.out.println(GenMethod.present(arr,2));
    }
}