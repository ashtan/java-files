
interface Box<T> {
    public T get();
    public void put(T element);
}

class MyBox<T> implements Box<T> {
   T item;

   public void put(T element) {
      item=element;
   }    
   public T get() {
      return item;
   }
}

class WCDemo 
{
   public static void unbox(Box<?> box) {
      System.out.println(box.get());
   }
   public static void main(String[] args) {
      MyBox<Integer> mb=new MyBox<>();
      mb.put(12);
      unbox(mb);
      MyBox<Double> mb2=new MyBox<>();
      mb2.put(3.14159);
      unbox(mb2);                
   }
}