

class GenMethod
{
    public static <T> boolean eq(T one, T two) {
        return one.equals(two);
    }
}

public class GenMethodTest {
    public static void main(String[] args) {
        System.out.println(GenMethod.eq(77,77));
    }
}