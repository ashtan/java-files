class SimpleGeneric<T>
{
    private T element;

    public T getElement() {
        return element;
    }
    public void setElement(T element) {
        this.element = element;
    }
}

public class SimpleDemo
{
    public static void main(String[] args) {
        SimpleGeneric<String> sg1 = new SimpleGeneric<String>();
        sg1.setElement("12345");
        System.out.println(sg1.getElement());
 
        SimpleGeneric<String> sg2 = new SimpleGeneric<String>();
        sg2.setElement("99");
        System.out.println(sg2.getElement());

    }
}