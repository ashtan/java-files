class SimpleGeneric<T> {
   private T element;
   public T getElement() { 
   	  return element;
   }
   public void setElement(T element) {
      this.element = element; 
   }
}

public class SimpleGenericTest {
   public static void main(String[] args) {
     SimpleGeneric<String> sg1 = new SimpleGeneric<>(); 
     sg1.setElement("12345");
     SimpleGeneric<Integer> sg2 = new SimpleGeneric<>();
     sg2.setElement(99);
     SimpleGeneric<Character> sg3 = new SimpleGeneric<>();
     sg3.setElement('\n');
   }
}