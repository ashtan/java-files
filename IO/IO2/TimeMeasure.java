import java.io.File;
import java.io.FileWriter;

public class TimeMeasure {
    public static void main(String[] args) throws Exception {
        long start = System.currentTimeMillis();
        long end = System.currentTimeMillis();
        System.out.println((end - start) / 1000f + " seconds");
    }
}
