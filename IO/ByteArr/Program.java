import java.io.*;

public class Program {
	public static void main(String[] arg) {
		byte [] arr={1,2,3,14,14,16,17,81,82,83};
		ByteArrayInputStream in=new ByteArrayInputStream(arr);
		ByteArrayOutputStream out=new ByteArrayOutputStream();

		System.out.println(in.available());

        int b;
       
		while((b=in.read())!=-1)
		   out.write(b);

		out.write(10);

        byte [] c=out.toByteArray();
        for(byte i:c)
		System.out.println((char)i);
	}
}