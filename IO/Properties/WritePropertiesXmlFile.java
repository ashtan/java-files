import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class WritePropertiesXmlFile {
	public static void main(String[] args) {
		try {
			Properties properties = new Properties();
			properties.setProperty("Animal", "Dog");
			properties.setProperty("Country", "Russia");
			properties.setProperty("Person", "Mama");

			File file = new File("test.xml");
			FileOutputStream fileOut = new FileOutputStream(file);
			properties.storeToXML(fileOut, "Favorite Things");
			fileOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}