import java.io.*;
import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class JSONRead {
	public static void main(String[] args) {
		try {

		   File f=new File("students.json");
		   JSONParser parser=new JSONParser();
		   FileReader fr = new FileReader(f);
		   Object obj=parser.parse(fr);
		   JSONObject js=(JSONObject)obj;
		   JSONArray items=(JSONArray)js.get("students");
		   for(Object i : items) {
		   	  System.out.print(((JSONObject)i).get("id"));
              System.out.println(((JSONObject)i).get("fio"));
		   }

		} catch (FileNotFoundException ex) {
			System.out.println(ex.getMessage());
		} catch (ParseException ex) {
			System.out.println(ex.getMessage());
		} catch(IOException ex) {
            System.out.println(ex.getMessage());
		}

	}
}