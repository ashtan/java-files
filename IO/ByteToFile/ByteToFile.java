
import java.io.*;

class ByteToFile {
    public static void main(String[] args) {

      try {
        byte buf [ ] = {'f','b','c','d'};
        ByteArrayOutputStream b = 
                      new ByteArrayOutputStream(); 
        b.write(buf);
        FileOutputStream f = 
                      new FileOutputStream("result.txt"); 
        b.writeTo(f);
        f.close();
        b.close();
      }
      /*catch(FileNotFoundException ex) {
        System.out.println("File not found!");
      }*/
      catch(IOException ex) {
        System.out.println("Input/output error! "+ex);
      }
    }
}