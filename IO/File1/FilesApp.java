import java.io.*;
public class FilesApp {
   public static void main(String[] args) {
     try
     {
        FileReader reader = new FileReader("1.txt");
        BufferedReader in = new BufferedReader(reader); 
        String line;
        while ((line=in.readLine()) != null) { 
        	System.out.println(line);
        }
        reader.close();
     }
     catch(FileNotFoundException ex){}
     catch(IOException ex){}

 }
}