import java.io.*;
import java.net.*;

public class Client {
  public static void main(String[] args) throws IOException {
    System.out.println("Клиент стартовал");
    Socket server = null;
    InputStream in = null;
    OutputStream out = null;
    File file = null;

    if (args.length==0) {
      System.out.println("Использование: java Client hostname");
      System.exit(-1);
    }

    System.out.println("Соединяемся с сервером "+args[0]);

    try {
      server = new Socket(args[0],1234);
    } 
    catch(IOException e) {
      System.out.println("Ошибка соединения с сервером");
      System.exit(-1);
    }

    file = new File("file.dat");
    long length = file.length();
    try {
        
       in = new FileInputStream(file); 
       out = server.getOutputStream();
       byte[] bytes = new byte[1024];
       int count;
       while ((count = in.read(bytes)) > 0) {
          out.write(bytes, 0, count);
          System.out.print("#");
       }
    }
    catch (IOException ex) {
      System.out.println("Ошибка ввода/вывода");
      System.exit(-1);
    }            
    
    System.out.println("Данные отправлены");
    System.out.println("Разрываем соединение");

    out.close();
    in.close(); 
    server.close();
  }
}