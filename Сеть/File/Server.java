import java.io.*;
import java.net.*;

public class Server {

  public static void main(String[] args) throws IOException {
    System.out.println("Старт сервера");
    InputStream  in = null;
    OutputStream out= null;

    ServerSocket server = null;
    Socket       client = null;

    // создаем серверный сокет
    try {
      server = new ServerSocket(1234);
    } catch (IOException e) {
      System.out.println("Ошибка связывания с портом 1234");
      System.exit(-1);
    }

    try {
      System.out.print("Ждем соединения");
      client= server.accept();
      System.out.println("Клиент подключился");
    } catch (IOException e) {
      System.out.println("Не могу установить соединение");
      System.exit(-1);
    }
  
    in  = client.getInputStream();
    try {
       out = new FileOutputStream("fromClient.txt");
    } catch(FileNotFoundException ex) {
       System.out.println("Ошибка создания файла!");
       System.exit(-1);
    }  
    byte[] data =new byte[1024];
    int count;

    System.out.println("Ожидаем данные");

    try {
      while ((count = in.read(data)) > 0) {
        out.write(data, 0, count);
        System.out.print("#");
      }
    }
    catch (IOException e) {
      System.out.println("Ошибка чтения/записи данных");
      System.exit(-1);
    }  
    System.out.println("Данные прочитаны");

    out.close();
    in.close();
    client.close();
    server.close();
  }
}