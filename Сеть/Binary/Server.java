import java.io.*;
import java.net.*;

public class Server {

  public static void main(String[] args) throws IOException {
    System.out.println("Старт сервера");
    InputStream  in = null;
    OutputStream out= null;

    ServerSocket server = null;
    Socket       client = null;

    // создаем серверный сокет
    try {
      server = new ServerSocket(1234);
    } catch (IOException e) {
      System.out.println("Ошибка связывания с портом 1234");
      System.exit(-1);
    }

    try {
      System.out.print("Ждем соединения");
      client= server.accept();
      System.out.println("Клиент подключился");
    } catch (IOException e) {
      System.out.println("Не могу установить соединение");
      System.exit(-1);
    }
  
    in  = client.getInputStream();
 
    byte[] responseBytes = new byte[15];
    byte[] len=new byte[1];
    int bytesRead = 0;
    try {
       in.read(len);
       System.out.println(len[0]);
       bytesRead = in.read(responseBytes);
       System.out.println(bytesRead);
    } catch (IOException e) {
       e.printStackTrace();
    }
    
    for(int i=0;i<bytesRead;i++)
       System.out.printf("%x\n",responseBytes[i]);

    
    //out.close();
    in.close();
    client.close();
    server.close();
  }
}