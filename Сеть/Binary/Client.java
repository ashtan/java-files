import java.net.*;
import java.io.*;

public class Client {

 
    public static void main(String[] args) {
       Socket socket;
       byte[] header = {0x53, 0x41, 0x4D, 0x50, 0x4C, 0x45}; 
       OutputStream out;
       InputStream in;
       BufferedOutputStream bufOut;
       
        try {
            socket = new Socket("127.0.0.1", 1234);
        } catch (Exception ee) {
            return;
        }

        try {
            out = socket.getOutputStream();
            bufOut = new BufferedOutputStream(out);
            in = socket.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
 
        byte msgSize=(byte)header.length;
 
        try {
            bufOut.write(msgSize);
            bufOut.flush();
            bufOut.write(header);
            bufOut.flush();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}