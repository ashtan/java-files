import java.io.*;
import java.net.*;

public class Client {
  public static void main(String[] args) throws IOException {
    System.out.println("Клиент стартовал");
    Socket server = null;

    if (args.length==0) {
      System.out.println("Использование: java Server_IP");
      System.exit(-1);
    }

    System.out.println("Соединяемся с сервером "+args[0]);

    try {
      server = new Socket(args[0],2345);

    } catch(UnknownHostException e) { 
      System.out.println("Неизвестный хост");
      System.exit(-1);      
    }
    catch(NoRouteToHostException e) {
      System.out.println("Нет связи");
      System.exit(-1);
    }
    catch(ConnectException e) {
      System.out.println("Ошибка соединения");
      System.exit(-1);
    }
    catch(IOException e) {
      System.out.println("Ошибка ввода/вывода");
      System.exit(-1);
    }

    BufferedReader in=null;
    PrintWriter out=null;
    BufferedReader inu=null;

    try {
      in= new BufferedReader(
       new  InputStreamReader(server.getInputStream()));
      out= new PrintWriter(server.getOutputStream(),true);
      inu= new BufferedReader(new InputStreamReader(System.in));
    }
    catch(IOException e) {
      System.out.println("Ошибка создания потоков");
      System.exit(-1);
    }
    String fuser,fserver;

    while ((fuser = inu.readLine())!=null) {
      if(fuser.length()==0)
        break;
      out.println(fuser);
      fserver = in.readLine();
      System.out.println(fserver);
      if (fuser.equalsIgnoreCase("exit")) 
        break;
    }

    out.close();
    in.close();
    inu.close();
    server.close();
  }
}
