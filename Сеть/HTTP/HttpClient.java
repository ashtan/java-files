import java.io.*;
import java.net.*;
 
public class HttpClient
{
    public static void main(String[] args) throws IOException 
    {
        Socket socket = new Socket();
        String host = "www.nniit.ru";
        PrintWriter out = null;
        BufferedReader in = null;
         
        try {
           socket.connect(new InetSocketAddress(host , 80));
           System.out.println("Соединение установлено");
           out = new PrintWriter(socket.getOutputStream(), true);
           in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        }
        catch (UnknownHostException e) 
        {
            System.err.println("Невозможно соединиться с: " + host);
            System.exit(1);
        }
         
        String message = "GET / HTTP/1.0\r\n\r\n";

        out.println( message );
             
        System.out.println("Сообщение послано");
        String response;
        while ((response = in.readLine()) != null) {
           System.out.println( response );
        }
    }
}