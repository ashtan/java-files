package anton;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Rule.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

//import junit.framework.Test;
//import junit.framework.TestCase;
//import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
   // extends TestCase
{
    private final ByteArrayOutputStream outContent =
            new ByteArrayOutputStream();
    //private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
       System.setOut(new PrintStream(outContent));
       //System.setErr(new PrintStream(errContent));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        //System.setErr(null);
    }

    @Test
    public void out() {
        System.out.print("hello");
        Assert.assertEquals("hello", outContent.toString());
    }

    @Test
    public void testAverage() throws Exception {
        int result=App.average(10,20);
        Assert.assertEquals(15,result);

    }

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    //public AppTest( String testName )
    //{
    //    super( testName );
    //}

    /**
     * @return the suite of tests being tested
     */
    //public static Test suite()
    //{
    //    return new TestSuite( AppTest.class );
    //}

    /**
     * Rigourous Test :-)
     */
    @Test
    public void testApp()
    {
        Assert.assertTrue( true);
    }


}

