import org.junit.Assert;

import static org.junit.Assert.*;

/**
 * Created by anton on 11.12.17.
 */
public class CalculatorTest {
    @org.junit.Test
    public void sum() throws Exception {
        Assert.assertEquals(4,Calculator.sum(2,2));
    }

    @org.junit.Test
    public void sub() throws Exception {
        Assert.assertEquals(0,Calculator.sub(2,2));

    }

    @org.junit.Test
    public void mul() throws Exception {
        Assert.assertEquals(400,Calculator.mul(20,20));

    }

}