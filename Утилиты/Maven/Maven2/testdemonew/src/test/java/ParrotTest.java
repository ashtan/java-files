import static org.junit.Assert.*;

/**
 * Created by anton on 26.11.17.
 */
public class ParrotTest {
    @org.junit.Test
    public void say() throws Exception {
        Parrot p=new Parrot("Кеша");
        String word=p.say("НИИТ");
        assertEquals("Кеша",word);
    }

    @org.junit.Test
    public void sayName() throws Exception {
        Parrot p=new Parrot("Кеша");
        String word=p.sayName();
        assertEquals("Кеша",word);
    }

}