/**
 * Created by anton on 26.11.17.
 */
public class Parrot {
    String name;
    public Parrot(String name) {
        this.name=name;
    }
    public String say(String word) {
        return word;
    }
    public String sayName() {
        return name;
    }
}
