package niit;

/**
 * Hello world!
 *
 */

class Automata
{
   private int state=0;
   public Automata(int state) {
       this.state=state;
   }
   public int getState() {
       return state;
   }
}

public class App 
{

    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
}
