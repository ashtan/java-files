public class Box {
    private int width; // ширина коробки 
    private int height; // высота коробки 
    private int depth; // глубина коробки
    // Конструктор
    public Box(int w,int h,int d) { 
        width = w;
        height = h;
        depth = d; 
    }
    public boolean equals(Box b) {
        return width==b.width &&
               height==b.height &&
               depth==b.depth;
    }
    // вычисляем объём коробки
    public int getVolume() {
         return width * height * depth;
    } 
    static
    {
        System.out.println("Hello from Box!");
    }
}