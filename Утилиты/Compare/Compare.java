public class Compare {
    static public void main(String[] args) {
        Box b1=new Box(2,3,3);
        Box b2=new Box(2,3,3);
        Box b3=b1;
        System.out.println("V1="+b1.getVolume());
        System.out.println("V2="+b2.getVolume());
        System.out.println(b1.equals(b2));
    }
    static
    {
        System.out.println("Hello from Compare!");
    }
}