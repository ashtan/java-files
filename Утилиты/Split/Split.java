class Split
{
    public static void main(String[] args) {
        if(args.length==0) {
            System.out.println("Usage: Split string");
            return;
        }
        String[] elements=args[0].split(",");
        for(String s:elements)
            System.out.println(s);
    }
}