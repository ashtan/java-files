package anton;

//import org.junit.Assert;
//import org.junit.Before;
import org.junit.Test;

import org.junit.runners.Parameterized;
//import org.junit.runners.Parameterized.Parameters;
import org.junit.runner.RunWith;

import java.util.Collection;
import java.util.Arrays;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class PrimeTest {

    boolean expected;
    int input;

    public PrimeTest (int in,boolean ex){
        this.expected=ex;
        this.input=in;
    }

    @Parameterized.Parameters
    public static Collection numbers()
    {
        return Arrays.asList(new Object[][] {
                {2,true},
                {3,true},
                {4,false},
                {5,true},
                {6,false},
                {7,true}});
    }

    @Test
    public void check() throws Exception {
        assertEquals(expected,Prime.check(input));
    }

}