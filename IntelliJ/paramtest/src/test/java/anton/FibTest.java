package anton;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by anton on 29.03.17.
 */

import org.junit.runners.Parameterized;
//import org.junit.runners.Parameterized.Parameters;
import org.junit.runner.RunWith;

import java.util.Collection;
import java.util.Arrays;


@RunWith(Parameterized.class)
public class FibTest {

    int n;
    int expected;

    public FibTest(int n,int expected) {
        this.n=n;
        this.expected=expected;
    }

    @Parameterized.Parameters
    public static Collection numbers()
    {
        return Arrays.asList(new Object[][] {
                {1,1},
                {2,1},
                {3,2},
                {4,3},
                {5,5},
                {6,8}});
    }

    @Test
    public void member() throws Exception {
          assertEquals(expected,Fib.member(n));
    }

}