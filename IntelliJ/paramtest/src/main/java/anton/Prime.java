package anton;

public class Prime
{
    public static boolean check(int val)
    {
        for(int i=2;i*i<=val;i++ )
        {
            if(val%i==0)
                return false;
        }
        return true;
    }
}