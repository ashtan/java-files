package anton;

public class Fib {
    public static int member(int n) {
        if(n==1 || n==2)
            return 1;
        else
            return member(n-1)+member(n-2);
    }
}