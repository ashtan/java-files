import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.*;

/**
 * Created by anton on 27.02.17.
 */
public class MainTest {
    @Test
    public void sub() throws Exception {
        int n=Main.sub(3,5);
        assertEquals(-2,n);
    }

    @org.junit.Test
    public void add() throws Exception {
        int n=Main.add(3,5);
        assertEquals(8,n);
    }

}