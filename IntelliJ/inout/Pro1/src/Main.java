import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException{
	   System.out.println("Hello, enter your name:");
	   String buf;
	   BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
       buf=in.readLine();
       System.out.println("Hello, "+buf);
       System.out.println("How old are you?");
       int age=0;
       try
       {
           age=Integer.parseInt(in.readLine());
       }
       catch(NumberFormatException ex) {
           System.out.println("Wrong age!");
           System.exit(1);
       }
       System.out.printf("Good age %d",age);


    }
}
