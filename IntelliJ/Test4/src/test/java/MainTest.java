import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by anton on 01.03.17.
 */
public class MainTest {
    @Test
    public void pow() throws Exception {
        int result=Main.pow(5,2);
        assertEquals(result,25);
    }

    @Test
    public void mod() throws Exception {
        int result=Main.mod(5,2);
        assertEquals(result,1);

    }

    @Test
    public void mul() throws Exception {
        int result=Main.mul(2,2);
        assertEquals(result,4);
    }
    @Test
    public void div() throws Exception {
        int result=Main.div(5,2);
        assertEquals(result,2);
    }
    @Test
    public void sub() throws Exception {
        int result=Main.sub(2,2);
        assertEquals(result,0);
    }
    @Test
    public void add() throws Exception {
        int result=Main.add(2,2);
        assertEquals(result,4);
    }

}