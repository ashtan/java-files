
public class Main
{
    public static int pow(int x,int y) {
        int result=x;
        for(int i=0;i<y;i++)
            result*=x;
        return result;
    }
    public static int mod(int x,int y) {
        return x%y;
    }
    public static int mul(int x,int y) {
        return x*y;
    }
    public static int div(int x,int y) {
        return x/y;
    }
    public static int sub(int x,int y) {
        return x-y;
    }
    public static int add(int x,int y) {
        return x+y;
    }
    public static void main(String[] args) {
        System.out.println("Hello");
    }
}