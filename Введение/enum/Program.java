enum Season
{
  WINTER("a"), 
  SPRING("b"), 
  SUMMER("c"), 
  AUTUMN("d");
  String number;
  Season(String number)
  {
     this.number=number;
  }

  public String get() 
  {
     return number;
  }
}

public class Program
{
   public static void main(String[] args)
   {
      Season now=Season.WINTER;
      System.out.println(now.get());
      System.out.println(now.ordinal());
      for(Season s:Season.values())
      {
         System.out.println(
              s.name()+":"+s.get());
      }
   }
}