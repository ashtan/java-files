public class MyNumber
{
   double number;
   double final precision=0.0000001;
   MyNumber(double number)
   {
      this.number=number;
   }
   double average(double x,double y)
   {
      return (x+y)/2.0;
   }
   boolean good(double guess,double x)
   {
      return Math.abs(guess*guess-x)<precision;
   }
   double improve(double guess, double x)
   {
      return average(guess,x/guess);
   }
   double iter(double guess,double x)
   {
      if(good(guess,x))
        return guess;
      else
        return iter(improve(guess,x),x);
   }
   public double sqrt()
   {
     return iter(1.0,number);
   }
}