
class Calculator
{
    private static final double pi=3.14159265;
    public static double getPi() {
        return pi;
    }
    public static int sum(int x,int y) {
        return x+y;
    }  
    public static int sub(int x,int y) {
        return x-y;
    }   
}

public class Hello
{
    public static void main(String[] args) {
      
    System.out.println("2+2="+Calculator.sum(2,2));
    System.out.println("PI="+Calculator.getPi());

    }
}
