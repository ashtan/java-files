class HelloWorld
{

    public static void main(String[] args) {
        Human vasya=new Human("Вася");
        vasya.say();
        Human petya=new Human("Петя");
        petya.toMars();
        petya.say();
        Human masha=new Human("Маша");
        masha.say();
    }
}

class Human
{
    private String name;
    private String planet="Земля";

    public Human(String name) {
        this.name=name;
    }
    public void toMars() {
        this.planet="Марс";
    }

    public void say() {
        System.out.println("Привет "+name);
        System.out.println("Я с планеты "+planet);
    }    
}