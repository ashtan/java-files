class ByeWorld {
    public void Message() {
        System.out.println("Bye Bye!");
    }
}

public class HelloWorld {
    public void Message() {
        System.out.println("Bye June!");
        ByeWorld hw = new ByeWorld();
        hw.Message();
    }

    public static void main(String args[]) {
        System.out.println("Hello,June!");
        HelloWorld hw = new HelloWorld();
        hw.Message();
    }
}
