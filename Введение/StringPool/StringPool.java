public class StringPool {

    /**
     * Java String Pool example
     * @param args
     */
    public static void main(String[] args) {
        String s1 = "Cat";
        String s2 = "Cat";
        String s3 = new String("Cat");
        String s4 = s3;
        
        System.out.println("s1 == s2 :"+(s1==s2));
        System.out.println("s1 == s3 :"+(s1==s3));

        System.out.println("s4 == s3 :"+(s4==s3));
        s2="Dog";
        s4="Dog";
        System.out.println("s4 == s3 :"+(s4==s3));

        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s3);


    }

}