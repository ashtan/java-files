enum ParrotColors {
  RED,GREEN,BLUE
}

class Parrot
{
    private ParrotColors color;
    private String name;
    private int age;
    public static boolean canFly=true;

    static {
      System.out.println("Parrot");
    }

    public Parrot(String name) {
        this(name,0,ParrotColors.BLUE);
    }
    public Parrot(String name,int age,ParrotColors color) {
              this.name=name;
              this.age=age;
              this.color=color;
    }
    public void setName(String name) {
      this.name=name;
    }
    public String getName() {
       return name;
    }
    public String getColor() {
      return color.toString();
    }
    public void say(String phrase) {
        System.out.println(name+":"+phrase);
    }
    public static void fly() {
        System.out.println(canFly?"Могу летать":"Не могу летать");

    }
    public Parrot getNewParrot(String name) {
        return new Parrot(name,age,color);
    }
}
public class ParrotDemo
{
    static {
      System.out.println("ParrotDemo");

    }
    public static void main(String[] args) {
      Parrot kesha1;
      System.out.println("Привет!");
      Parrot kesha=new Parrot("Кеша",2,ParrotColors.RED);
      Parrot gosha=new Parrot("Гоша",6,ParrotColors.GREEN);
      System.out.println(kesha.getName()+" "+kesha.getColor());
      System.out.println(gosha.getName()+" "+gosha.getColor());
      kesha.fly();
      kesha.canFly=false;
      gosha.fly();
      Parrot vasya=kesha.getNewParrot("Вася");
      System.out.println(vasya.getName());
      System.out.println(vasya.getColor());
      vasya.fly();
    }
}
