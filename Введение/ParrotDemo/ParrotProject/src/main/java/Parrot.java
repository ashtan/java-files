enum ParrotColors {
    RED,GREEN,BLUE
}

public class Parrot {
    private ParrotColors color;
    private String name;
    private int age;
    public static boolean canFly=true;

    static {
        System.out.println("Parrot");
    }

    public Parrot(String name) {
        this(name,0,ParrotColors.BLUE);
    }
    public Parrot(String name,int age,ParrotColors color) {
        this.name=name;
        this.age=age;
        this.color=color;
    }
    public void setName(String name) {
        this.name=name;
    }
    public String getName() {
        return name;
    }
    public String getColor() {
        return color.toString();
    }
    public void say(String phrase) {
        System.out.println(name+":"+phrase);
    }
    public static void fly() {
        System.out.println(canFly?"Могу летать":"Не могу летать");

    }
    public Parrot getNewParrot(String name) {
        return new Parrot(name,age,color);
    }
}
