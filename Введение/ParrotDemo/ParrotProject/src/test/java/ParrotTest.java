import static org.junit.Assert.*;

public class ParrotTest {

    @org.junit.Test
    public void setName() {
        Parrot p1=new Parrot("Vasya");
        p1.setName("Petya");
        String result=p1.getName();
        assertTrue(result.equals("Petya"));
    }

    @org.junit.Test
    public void getName() {
        Parrot p1=new Parrot("Vasya");
        String result=p1.getName();
        assertTrue(result.equals("Vasya"));
    }
}