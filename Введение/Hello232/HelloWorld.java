
class MyString {
   public String text="Hello!";
}

public class HelloWorld {

    private static String text="Hello!"; 

    public static void change(MyString str) {
        str.text="Bye!";
    }
    public static void main(String[] args) {
        MyString mystring=new MyString();
        System.out.println(mystring.text);
        change(mystring);
        System.out.println(mystring.text);
        

    }
}