import java.util.ArrayList;

enum ParrotColors {GREEN, BLUE, RED, MIXED};

class Parrot
{
    String name;
    ParrotColors color;
    boolean canspeak;
    ArrayList<String> words;
    String learnword;

    public Parrot(String name,
                  ParrotColors color,
                  boolean canspeak)
    {
        this.name=name;
        this.color=color;
        this.canspeak=canspeak;
        words=new ArrayList<String>();
    }
    public String getName() {
        return name;
    }
    public boolean canSpeak() {
        return canspeak;
    }
    public void learn(String word) {
        learnword=word;
        words.add(word);
    }
    public void speak(String word) {
        if(words.contains(word))
            System.out.println(word);
    }
    public void talk() {
        for(String word: words) {
            System.out.println(word);
        }
    }
    public void printColor() {
        System.out.println(color.toString());
    }
    public void setStringColor(String colname) {
        color=ParrotColors.valueOf(colname);
    }
}

public class ParrotDemo
{
    public static void main(String[] args)
    {
        Parrot parrot1=new Parrot("Кеша",
                                   ParrotColors.BLUE,
                                   true);
        parrot1.printColor();
        parrot1.setStringColor("GREEN");
        parrot1.printColor();


    }
}