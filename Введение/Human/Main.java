
class Person {
    String name;
    int year;
    String position;
    static String company;

    public void info() {
        System.out.println(name+","+
                           year+","+
                           position+","+
                           company);
    }
}

public class Main {
    public static void main(String[] args) {
        Person.company=args[0];
        Person person1=new Person();
        person1.name="Alex";
        person1.year=1995;
        person1.position="developer";
        person1.info();
        //Person.company="Oracle";
        Person person2=new Person();
        person2.name="Misha";
        person2.year=1980;
        person2.position="senior developer";
        person2.info();

    }
}