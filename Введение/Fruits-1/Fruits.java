import java.util.Arrays;

enum Fru {
  APPLE,BANANA,QIWI,ORANGE,ЯБЛОКО
}

public class Fruits {
    String[] list = {"Apple","Apple","Apple"};

    static {
      System.out.println("static");
    }
    Fruits() {
      list[0]="Apple";
      list[1]="Orange";
      list[2]="Banana";
    }
    public static void main(String[] args) {
      Fruits fr=new Fruits();

      System.out.println(Fruits.class.getSuperclass());
      System.out.println(Fru.class.getSuperclass());
        for(int i=0;i<fr.list.length;i++)
            System.out.println(fr.list[i]);

      String f="яблоко";
      Fru favourite=Fru.BANANA;
      //System.out.println(favourite.name());
      System.out.println(favourite.valueOf(f.toUpperCase()).ordinal());
      System.out.println(Fru.APPLE.ordinal()<Fru.BANANA.ordinal());

      int[] arr={1,2,3,4,5};
      System.out.println(Arrays.toString(arr));

    }
}