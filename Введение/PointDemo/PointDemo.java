class Point {
    private int x, y; // экземплярные переменные 
    public Point() { // конструктор
      this(0,0);
    }
    public Point (int x,int y) { // конструктор 
        this.x=x; this.y=y;
    }
    public int getX() {return x;} 
    public int getY() {return y;}
}

class PointDemo {
    public static void main(String[] args) {
        Point center=new Point();
        Point p1=new Point(1,1);
        System.out.println("("+p1.getX()+","+p1.getY()+")");
        //System.out.println("("+p1.x+","+p1.y+")");

    }
}