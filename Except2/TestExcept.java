class TestExcept {
  public static void main(String[] args) {
    String[] names=new String[] {"Маша", "Саша", "Надя"};

    try {
        for(int i=0;i<names.length;i++)
          System.out.println(names[i]);
        System.out.println("Выиграли в лотерею");
        throw new ArithmeticException();
    }
    catch(ArrayIndexOutOfBoundsException ex) {
      System.out.println("Вышли за границу массива");
    }
    //catch(ArithmeticException ex) {
    //  System.out.println("Деление на 0!");
      //throw new ArithmeticException();
    //}
    finally {
      System.out.println("Finally");
    }
    System.out.println("main");
  }
}
