/**
 * Created by anton on 09.05.17.
 */
import java.io.IOException;
import java.net.URL;

public class Main {
    public static void main(String[] args) throws IOException {
        String from = "USD";
        String to = "EUR";

        java.net.URL url = new java.net.URL(
           "http://www.webservicex.net/CurrencyConvertor.asmx"
                        + "/ConversionRate?FromCurrency=" + from
                        + "&ToCurrency=" + to);
        java.util.Scanner sc = new java.util.Scanner(url.openStream());

        // <?xml version="1.0" encoding="utf-8"?>
        sc.nextLine();

        // <double xmlns="http://www.webserviceX.NET/">0.724</double>
        String str = sc.nextLine().replaceAll("^.*>(.*)<.*$", "$1");

        sc.close();

        Double rate = Double.parseDouble(str);
        System.out.println(rate);

    }
}
