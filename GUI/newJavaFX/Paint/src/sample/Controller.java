package sample;

import javafx.fxml.FXML;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.canvas.Canvas;

import java.awt.*;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javafx.scene.paint.Color;

public class Controller {

    public Timer timer;
    public TimerTask task;

    @FXML
    public Canvas theCanvas;
    private Random rnd;

    @FXML
    public void initialize() {
        final GraphicsContext gc=theCanvas.getGraphicsContext2D();
        timer=new Timer();
        rnd=new Random();

        task=new TimerTask() {
            @Override
            public void run() {
                double x=(double)rnd.nextInt(500);
                double y=(double)rnd.nextInt(300);
                Color c=Color.rgb(rnd.nextInt(256),
                        rnd.nextInt(256),
                        rnd.nextInt(256));
                gc.setFill(c);
                gc.fillOval(x,y,50,50);


            }
        };
        timer.schedule(task,0,100);
    }
}
