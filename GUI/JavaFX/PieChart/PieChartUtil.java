
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
public class PieChartUtil {
   public static ObservableList<PieChart.Data> getChartData() {
      ObservableList<PieChart.Data> data = FXCollections. observableArrayList();
      data.add(new PieChart.Data("საქართველო",3.7));
      data.add(new PieChart.Data("中華人民共和國", 1275));
      data.add(new PieChart.Data("India", 1017));
      data.add(new PieChart.Data("Brazil", 172));
      data.add(new PieChart.Data("UK", 59));
      data.add(new PieChart.Data("USA", 285));
      data.add(new PieChart.Data("Россия", 146));
      data.add(new PieChart.Data("Iceland", 0.3));
      data.add(new PieChart.Data("Германия",80));
      data.add(new PieChart.Data("Парагвай",6.7));
      data.add(new PieChart.Data("Италия",70));


      return data;
   }
}
