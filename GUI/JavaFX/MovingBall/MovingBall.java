import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.geometry.Bounds;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.scene.canvas.*;
import javafx.scene.*;
import javafx.scene.shape.*;
import javafx.animation.*;

public class MovingBall extends Application{

    @Override
    public void start(Stage stage) {
    	Pane canvas = new Pane();
    	Scene scene = new Scene(canvas, 500, 400);
        Group group = new Group();
    	Circle ball = new Circle(10, Color.RED);
        Rectangle rect=new Rectangle(10,40,20,80);
        rect.setFill(Color.VIOLET);

        ball.relocate(0, 10);

        canvas.getChildren().add(ball);
        canvas.getChildren().add(rect);

        stage.setTitle("Moving Ball");
        stage.setScene(scene);
        stage.show();

        

        Bounds bounds = canvas.getBoundsInLocal();
        Timeline timeline = new Timeline(
            new KeyFrame(Duration.seconds(30),
               new KeyValue(ball.layoutXProperty(), bounds.getMaxX()-ball.getRadius())));
        
        Path p=new Path();
        p.getElements().add(new MoveTo(10,10));
        p.getElements().add(new MoveTo(100,100));

        p.getElements().add(new CubicCurveTo(180,0,180,100,150,100));
        p.getElements().add(new CubicCurveTo(0,100,0,200,180,200));

        PathTransition pt=new PathTransition();   
        pt.setPath(p);
        pt.setNode(ball);
        pt.setDuration(Duration.millis(4000));
        pt.setCycleCount(Timeline.INDEFINITE);
        pt.setAutoReverse(true);

        pt.play();

        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setAutoReverse(true);
        timeline.play();
    }

    public static void main(String[] args) {
        launch();
    }
}