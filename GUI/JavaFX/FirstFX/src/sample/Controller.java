package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class Controller {

    @FXML
    private Button btnFirst;
    @FXML
    private Button btnSecond;
    @FXML
    private Button btnThird;

    @FXML
    public void clickFirst() {
        btnFirst.setText("нажали");
    }
    @FXML
    public void clickSecond() {
        btnSecond.setText("нажали");
    }
    @FXML
    public void clickThird() {
        btnThird.setText("нажали");
    }
}
