package sample;

import eu.hansolo.tilesfx.Tile;
import eu.hansolo.tilesfx.TileBuilder;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.ResourceBundle;
import eu.hansolo.medusa.*;


public class Controller implements Initializable {

    @FXML
    private Button buttonAdd;
    @FXML
    private TextField tfId;
    @FXML
    private TextField tfFio;
    @FXML
    private TextField tfGroup;
    @FXML
    private Pane paneClock;


    @FXML
    private TextField tfGroupFilter;
    @FXML
    private Button buttonFilter;


    @FXML
    private TableView<Student> studTableView;
    @FXML
    private TableColumn<Student,Integer> idColumn;
    @FXML
    private TableColumn<Student,String> fioColumn;
    @FXML
    private TableColumn<Student,String> groupColumn;


    // основной список с данными
    private ObservableList<Student> studData = FXCollections.observableArrayList();
    // фильтрованный список
    private FilteredList<Student> filtData;


    private Clock clockTile;

    @FXML
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        clockTile=ClockBuilder.create()
                .skinType(Clock.ClockSkinType.DESIGN)
                .secondsVisible(true)
                .running(true)
                .build();

        paneClock.getChildren().add(clockTile);

        idColumn.setCellValueFactory(new PropertyValueFactory<Student,Integer>("id"));
        fioColumn.setCellValueFactory(new PropertyValueFactory<Student,String>("fio"));
        groupColumn.setCellValueFactory(new PropertyValueFactory<Student,String>("group"));


        studData.add(new Student(1,"Иванов А.А.","A-123"));
        studData.add(new Student(2,"Петрова В.В.","A-123"));
        studData.add(new Student(3,"Сидоров Д.Д.","A-456"));
        studData.add(new Student(4,"Сидорова Я.Я.","A-123"));
        studData.add(new Student(5,"Сидорин И.И.","A-456"));


        studTableView.setItems(studData);

        buttonAdd.setOnAction(e -> {
            int id=Integer.parseInt(tfId.getText());
            String fio=tfFio.getText();
            String group=tfGroup.getText();
            studData.add(new Student(id,fio,group));
        });

        buttonFilter.setOnAction(event -> {
            filtData=new FilteredList<Student>(studData, student -> {
                if(student.getGroup().equals(tfGroupFilter.getText()))
                   return true;
                else
                   return false;
            });
            studTableView.setItems(filtData);
        });

    }




}
