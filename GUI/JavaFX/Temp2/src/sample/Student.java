package sample;

/**
 * Created by anton on 21.01.18.
 */
public class Student
{
    private int id;
    private String fio;
    private String group;

    public Student(int id,String fio,String group) {
        this.id=id;
        this.fio=fio;
        this.group=group;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getFio() {
        return fio;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getGroup() {
        return group;
    }
}
