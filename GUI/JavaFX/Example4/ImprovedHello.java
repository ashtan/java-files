import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
public class ImprovedHello extends Application {
       private int space=0;
       public static void main(String[] args) {
               Application.launch(args);
       }
       @Override
       public void start(Stage stage) {

    
       VBox root = new VBox();
       HBox buttons = new HBox();
              
               Button btn1 = new Button("Say Hello");
               Button btn2 = new Button("Exit");
               Button btn3 = new Button("Enter");
               Button btn4 = new Button("Bye!");

              
               btn1.setOnAction(e -> {
                 buttons.setSpacing(space+=10);
               });
       
       btn4.setOnAction(e -> Platform.exit());

       
       buttons.getChildren().addAll(btn1, btn2,btn3,btn4);
       root.getChildren().addAll( buttons);
       Scene scene = new Scene(root, 500, 550);
       stage.setScene(scene);
       stage.setTitle("Издевательство");
       stage.show();
   }
}
