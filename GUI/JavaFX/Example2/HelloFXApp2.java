import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.control.Button;
import javafx.stage.Stage;
public class HelloFXApp2 extends Application {
       public static void main(String[] args) {
               Application.launch(args);
       }
       @Override
       public void start(Stage stage) {
          Text msg1 = new Text("Hello JavaFX");
          Text msg2 = new Text("Hello Java");
          VBox root = new VBox();
          Button btn=new Button("Press me");
          root.getChildren().addAll(msg1,msg2,btn);
          
          Scene scene = new Scene(root, 300, 50);
          stage.setScene(scene);
          stage.setTitle("Hello JavaFX Application with a Scene");
          stage.show();
      }
}