package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

import java.time.LocalDate;

public class Controller {
    @FXML
    private Button btn1;

    @FXML
    private Button btn2;

    @FXML
    private TextField text1;

    @FXML
    private Button btn3;

    @FXML
    private DatePicker date1;

    @FXML
    public void clickBtn1() {
        System.out.println("Hello from button1!");
    }
    @FXML
    public void clickBtn2() {
        System.out.println("Hello from button2!");
    }
    @FXML
    public void clickBtn3() {
        System.out.println(text1.getText());
    }
    @FXML
    public void datePick() {
        System.out.println(date1.getEditor().getText());
        LocalDate value=date1.getValue();
        System.out.println(value.getDayOfYear());

    }
}
