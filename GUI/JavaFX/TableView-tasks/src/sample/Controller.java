package sample;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import javax.swing.*;

import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

import java.awt.*;
import java.util.Observable;


public class Controller {

    private ObservableList<Task> data= FXCollections.observableArrayList();

    @FXML
    private TextField newTask;

    @FXML
    private TableView<Task> table;

    @FXML
    private TableColumn<Task,Integer> idCol;

    @FXML
    private TableColumn<Task,String> titleCol;

    @FXML
    private TableColumn<Task, Boolean> doneCol;

    @FXML
    private void initialize() {
        idCol.setCellValueFactory(new PropertyValueFactory<Task, Integer>("id"));
        titleCol.setCellValueFactory(new PropertyValueFactory<Task, String>("title"));
        doneCol.setCellValueFactory(new PropertyValueFactory<>("selected"));


        doneCol.setCellFactory(col -> {
            CheckBoxTableCell<Task, Boolean> cell =
                    new CheckBoxTableCell<>(index -> {
                BooleanProperty active = new SimpleBooleanProperty(
                        table.getItems().get(index).isSelected());
                active.addListener((obs, wasActive, isNowActive) -> {
                    Task item = table.getItems().get(index);
                    item.setSelected(isNowActive);
                    System.out.println(item.getId());
                    //table.getItems().get(index).
                });
                return active ;
            });
            return cell ;
        });


        table.setItems(data);
    }

    public void addClicked() {
        int id=data.size()+1;
        data.add(new Task(id,newTask.getText()));

    }

}
