/**
 * Created by anton on 06.06.17.
 */

package sample;

import javafx.beans.property.SimpleBooleanProperty;

public class Task {
    private int id;
    private String title;
    private SimpleBooleanProperty selected;

    public Task(int id, String title) {
        this.id=id;
        this.title=title;
        this.selected=new SimpleBooleanProperty(false);

    }
    public int getId() {
        return id;
    }
    public String getTitle() {
        return title;
    }
    public SimpleBooleanProperty SelectedProperty() {
        return selected;
    }
    public void setSelected(boolean selected) {
        this.selected.set(selected);
    }
    public boolean isSelected() {
        return selected.get();
    }
}
