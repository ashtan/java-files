import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import java.time.LocalTime;

public class AnalogClock extends JComponent implements Runnable
{
  public AnalogClock()  {
    (new Thread(this)).start();
  }

  public void run() {
    try {
      for(;;) {
        Thread.sleep(500);
        repaint();
      }
    }
    catch (InterruptedException e) {
      Thread.currentThread().interrupt();
    }
  }
  public void paint(Graphics graphics) {
    super.paint(graphics);

    LocalTime lt=LocalTime.now();

    long minute=lt.getMinute();
    long sec = lt.getSecond();
    long hour = lt.getHour();
    //System.out.println(hour+" "+minute+" "+sec);

    double sec_angle = -Math.PI/2+sec*6.0*Math.PI/180;
    double min_angle = -Math.PI/2+6.0*Math.PI/180*
                     (minute+sec/60.0);
    double hour_angle = -Math.PI/2+30.0*Math.PI/180*
                    (hour+minute/60.0);

    // Draw the hands
    Graphics2D g = (Graphics2D) graphics;
    g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
              RenderingHints.VALUE_ANTIALIAS_ON);
    size = getSize(size);
    int radius =
    Math.min(size.width,size.height) / 2;
    g.translate((double)size.width/2D,(double)size.height/2D);

    // draw the seconds
    g.setColor(Color.red);
    g.setStroke(SEC_STROKE);
    g.rotate(sec_angle);
    g.drawLine(0, 0, radius - 20, 0);
    g.rotate(-sec_angle);

    // draw the minutes
    g.setColor(Color.blue);
    g.setStroke(MIN_STROKE);
    g.rotate(min_angle);
    g.drawLine(0, 0, radius - 20, 0);
    g.rotate(-min_angle);

    // draw the hours
    g.setColor(Color.black);
    g.setStroke(HOUR_STROKE);
    g.rotate(hour_angle);
    g.drawLine(0, 0, radius -40, 0);
    g.rotate(-hour_angle);

    // draw the perimeter
    g.setColor(Color.darkGray);
    g.drawOval(-radius + 2, -radius + 2, 2 * radius - 4, 2 * radius - 4);

    for (int j = 0; j < 60; ++j) {
         if ((j % 5) == 0)
             g.drawLine(radius-20, 0, radius, 0);
         else
             g.drawLine(radius-10, 0, radius, 0);
         g.rotate(6.0*Math.PI/180);
     }
    }


  private static Stroke SEC_STROKE = new BasicStroke();
  private static Stroke MIN_STROKE =
    new BasicStroke(4F, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
  private static Stroke HOUR_STROKE = MIN_STROKE;

  private Dimension size = null;

  public static void main(String[] args)
    {
       JFrame f = new JFrame("Analog Clock");
       AnalogClock clock = new AnalogClock();
       f.add(clock);
       f.addWindowListener(new WindowAdapter() {
          public void windowClosing(WindowEvent e) {
             System.exit(0);
          }
       });
       f.setBounds(50, 50, 400, 400);
       f.show();
    }

 }
