import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Simple 
{
   public static void main (String [] args) {
      JFrame myWindow = new JFrame("Пробное окно");
      myWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      myWindow.setSize(400, 300);
   
      myWindow.setLayout(new FlowLayout());

      ImageIcon ic = new ImageIcon("lambda.png");
      JButton btn1 = new JButton("Привет!");
      JButton btn2 = new JButton("Пока");
      JTextField tf = new JTextField(10);
      JCheckBox chb = new JCheckBox();
      chb.setSelected(true);
      chb.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent ae) {
            if(chb.isSelected()==false) {
              String str=tf.getText();
              JOptionPane.showMessageDialog(null,str,"Нажато",JOptionPane.WARNING_MESSAGE,ic);
            }
         }
      });

      tf.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent ae) {
            String str=tf.getText();
            JOptionPane.showMessageDialog(null,str,"Нажато",JOptionPane.WARNING_MESSAGE);
         }
      });

      btn1.addActionListener(new ActionListener() {
      	public void actionPerformed(ActionEvent ae) {
      		JOptionPane.showMessageDialog(null,"Привет","Нажато",JOptionPane.WARNING_MESSAGE);
      	}
      });
      btn2.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent ae) {
            myWindow.setSize(100,100);
         }
      });      

      myWindow.add(btn1);
      myWindow.add(btn2);
      myWindow.add(tf);
      myWindow.add(chb);
      myWindow.setVisible(true);
   }
}