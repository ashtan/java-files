import javax.swing.*; 
import java.awt.*; 
import java.awt.event.*;

public class Simple {
    public static void main (String [] args) {
      JFrame myWindow = new JFrame("Пробное окно");
      myWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
      myWindow.setSize(400, 300);
      myWindow.setLayout(new FlowLayout()); 

      JButton btn1 = new JButton("Привет!");
      JButton btn2 = new JButton("Пока!");
      JButton btn3 = new JButton("qwer!");
      JButton btn4 = new JButton("1234!");

      btn1.addActionListener(new ActionListener() { 
        public void actionPerformed(ActionEvent ae) {
           JOptionPane.showMessageDialog(null,"Привет","Нажато", JOptionPane.WARNING_MESSAGE);
        } 
    });
    JPanel jp1=new JPanel();
    FlowLayout dop1=new FlowLayout();
    jp1.setLayout(dop1);
    jp1.add(btn1);
    jp1.add(btn2);

    JPanel jp2=new JPanel();
    BorderLayout dop2=new BorderLayout();
    jp2.setLayout(dop2);

    jp2.add(btn3);
    jp2.add(btn4);
    myWindow.add(jp1);
    myWindow.add(jp2);

    myWindow.setVisible(true); 
   }
}