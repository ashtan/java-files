import javax.swing.*; 
import java.awt.*; 
import java.awt.event.*;

class LoginWindow extends JFrame implements ActionListener
{
    private JTextField login;
    private JPasswordField pass;
    private JButton btn;

    LoginWindow(String title)
    {
        super(title);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 300);
        setLayout(new FlowLayout()); 

        login=new JTextField(10);
        pass=new JPasswordField(10);
        btn=new JButton("Ввод");

        btn.setActionCommand("LOGIN");
        btn.addActionListener(this);

        add(login);
        add(pass);
        add(btn);
        

    }
    public void actionPerformed(ActionEvent evt) {
        String cmd = evt.getActionCommand(); 
        if(cmd.equals("LOGIN")) {
           String p=new String(pass.getPassword());
           if(p.equals("secret"))
              JOptionPane.showMessageDialog(null,"Доступ разрешен","", JOptionPane.WARNING_MESSAGE);
           else
           {
             JOptionPane.showMessageDialog(null,"Доступ запрещен","", JOptionPane.WARNING_MESSAGE);
             System.exit(0);
           }
        }
    }
}


public class Simple {
   public static void main (String [] args) {
     LoginWindow myWindow = new LoginWindow("Вход"); 
         myWindow.setVisible(true);
     }
 }