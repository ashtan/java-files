import java.io.*;
import java.lang.*;
import java.text.*;
import java.util.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import java.util.Calendar;
import java.util.Date;



public class JRemain extends JPanel implements ActionListener{
  private int count;
  public static JProgressBar pb;
  javax.swing.Timer timer=new javax.swing.Timer(500,timerFun());
  JLabel text1,text2,text3;


   public void actionPerformed(final ActionEvent e) {
        if (e.getActionCommand().equals("Start")) {
	         pb.setValue(0);
           timer.start();
        }
        else if (e.getActionCommand().equals("Stop")) {
          timer.stop();
	      }
	      else if (e.getActionCommand().equals("Clear")) {
          timer.stop();
	        pb.setValue(0);
          count=0;
	      }
    }

  public Action timerFun() {
	  return new AbstractAction() {
		  public void actionPerformed (ActionEvent e) {
          if(pb.getValue()<100)
	     		   pb.setValue(++count);
      			else 
	      		 timer.stop();
      }
	  };
  }

  public static void main( String[] args ) throws IOException   {  
    //Look.setNativeLookAndFeel();
    Look.setMotifLookAndFeel();    
    //Look.setJavaLookAndFeel();
    JFrame frame = new JFrame( "JRemain" );
    JRemain app=new JRemain();

    final JButton button1=new JButton("Start");
    button1.setActionCommand("Start");
    button1.addActionListener(app);

    final JButton button2=new JButton("Stop");
    button2.setActionCommand("Stop");
    button2.addActionListener(app);

    final JButton button3=new JButton("Clear");
    button3.setActionCommand("Clear");
    button3.addActionListener(app);

    app.text1=new JLabel("");
    app.text2=new JLabel("");
    app.text3=new JLabel("");


    pb=new JProgressBar(JProgressBar.HORIZONTAL,0,100);
    /* {
	    public Dimension getPreferredSize() {
		    return new Dimension(400,super.getPreferredSize().height*2);
	    }
    };*/

    pb.setStringPainted(true);

    final JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        buttonPanel.add(button1);
        buttonPanel.add(button2);
        buttonPanel.add(button3);
	      buttonPanel.add(pb);

    final JPanel mainPanel=new JPanel(new GridLayout(1,1));
        mainPanel.add(buttonPanel);


    JPanel mpanel=new JPanel(new BorderLayout());
    mpanel.add(mainPanel);

    frame.add(mpanel);
    frame.setSize( 800, 150 );
    frame.setVisible( true );
    frame.setResizable(false);
    frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
  }
}
