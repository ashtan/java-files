import javax.swing.*; 
import java.awt.*; 
import java.awt.event.*;

class MyWindow extends JFrame implements ActionListener {
    JTextField txt;

    public MyWindow(String title) {
        super(title);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 300);
        setLayout(new GridLayout(3,1)); 
        JButton btn1 = new JButton("Привет!");
        JButton btn2 = new JButton("Пока!");
        txt = new JTextField(10);

       btn1.addActionListener(this);
       btn1.setActionCommand("B1");
       btn2.addActionListener(this);
       btn2.setActionCommand("B2");
       txt.addActionListener(this);
       txt.setActionCommand("TXT");

       add(btn1);
       add(btn2);
       add(txt);
    }
    public void actionPerformed(ActionEvent evt) {
       String cmd = evt.getActionCommand();
       if(cmd.equals("B1"))
         JOptionPane.showMessageDialog(null,"Привет","Нажато", JOptionPane.WARNING_MESSAGE);
       else if(cmd.equals("B2"))
         JOptionPane.showMessageDialog(null,"Пока","Нажато", JOptionPane.WARNING_MESSAGE);
       else 
        JOptionPane.showMessageDialog(null,txt.getText(),"Нажато", JOptionPane.WARNING_MESSAGE);
   }
}


public class Simple {
   public static void main (String [] args) {
       MyWindow myWindow = new MyWindow("Пробное окно"); 
       myWindow.setVisible(true);
   }
   
}