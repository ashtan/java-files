import java.awt.*;
import javax.swing.*;


class TestSwing
{
   public static void main(String[] args)
   {
      JFrame f = new JFrame();
      Container cp = f.getContentPane();
      f.setBounds(100,100,300,300);
      String s[ ] = {"1","2","3","4","5","6","7","8","9","10"};
      JList jl = new JList(s);
      jl.setBounds(10,10,20,20);
      JScrollPane p = new JScrollPane(jl);
      cp.add(p); 
      f.setVisible(true);

   }
}