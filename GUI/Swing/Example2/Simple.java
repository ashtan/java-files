import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Simple
{
   public static void main (String [] args) {
      
      JFrame myWindow = new JFrame("Пробное окно");

      myWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      myWindow.setSize(700, 500);


      myWindow.setLayout(new GridLayout(2,2));

      JLabel lab = new JLabel("Привет!");
      myWindow.add(lab);

      JLabel lab2 = new JLabel("С Новым годом!");
      myWindow.add(lab2);
      JLabel lab3 = new JLabel("С Днем весны и труда!");
      myWindow.add(lab3);

      ImageIcon icon=new ImageIcon("blue.png");
      JButton btn1 = new JButton("С Днем весны и труда!",icon);
      myWindow.add(btn1);
      btn1.addActionListener(new ActionListener() { 
         public void actionPerformed(ActionEvent ae) {
             JOptionPane.showMessageDialog(null,"Привет","Нажато", JOptionPane.WARNING_MESSAGE,icon);
      }  });

      myWindow.setVisible(true);


   }
}
