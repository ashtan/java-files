import static org.junit.Assert.*;

/**
 * Created by anton on 03.12.17.
 */
public class AutomataTest {
    @org.junit.Test
    public void on() throws Exception {
        Automata a=new Automata();
        a.on();
        assertEquals("WAIT",a.getState());
    }

    @org.junit.Test
    public void off() throws Exception {
        Automata a=new Automata();
        a.on();
        a.off();
        assertEquals("OFF",a.getState());
    }

    @org.junit.Test
    public void getState() throws Exception {
        Automata a=new Automata();
        assertEquals("OFF",a.getState());
    }

}