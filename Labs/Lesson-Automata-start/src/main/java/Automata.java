/**
 * Created by anton on 03.12.17.
 */

enum STATES {OFF,WAIT,ACCEPT,COOK};

public class Automata {
    private int deposit;
    private STATES state;
    Automata() { state=STATES.OFF;}
    public void on() {
        if(state==STATES.OFF)
            state=STATES.WAIT;
    }
    public void off() {
        if(state==STATES.WAIT)
            state=STATES.OFF;
    }
    public String getState() {
        return state.toString();
    }
}
