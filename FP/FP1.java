import java.util.ArrayList;
//import java.util.stream;
import java.util.stream.IntStream; 

class FP1
{

    public Integer transform(Integer value) {
       value*=value;
       return value;
    }

    public static void main(String[] args) {
        ArrayList<Integer> test=new ArrayList<Integer>();
        FP1 fp1=new FP1();
        test.add(1);
        test.add(2);
        test.add(3);
        test.add(4);
        test.add(5);
        test.add(6);

        //test.stream().map(fp1::transform);
        test.forEach(fp1::transform);
        for(Integer i : test)
            System.out.println(i);

        IntStream.range(1, 400).forEach(s -> System.out.print(s+" "));

    }
}