import java.util.ArrayList;
import java.util.function.*;
import java.util.stream.*;

class FP2
{

    public static Integer summa(ArrayList<Integer> x,
                                Function<Integer,Integer> f)
    {
       Integer s=0;
       for(Integer i: x)
        s+=f.apply(i);
       return s;
    }

    public static void main(String[] args) {
        ArrayList<Integer> test=new ArrayList<Integer>();
 
        test.add(1);
        test.add(2);
        test.add(3);
        test.add(4);
        test.add(5);
        test.add(6);

        Integer y=summa(test,x->{return x*x*x;})-
                  summa(test,x->{return x*x;});
        System.out.println(y);

        Stream.of("d2", "a2", "b1", "b3", "c")
             .filter(s -> {
                             System.out.println("filter: " + s);
                             return true;
                           })
             .forEach(s -> System.out.println(s));
    }
}
