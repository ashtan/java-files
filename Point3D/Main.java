class Point {
   public int x; 
   public int y;
}
class Point3D extends Point {
   public int z; 
}

public class Main {
    public static void main(String[] args) {
       Point Pobj = new Point();
       Point3D Cobj = new Point3D();
       Pobj = Cobj;
       Pobj.x = 1; //верно! x определена в Point 
       //Pobj.z = 10; //ошибка! z не определена в Point 
       
       Cobj=(Point3D)Pobj;
       System.out.println(Cobj.z);
       Cobj.z=10;
       System.out.println(Cobj.z);


    }
} 
