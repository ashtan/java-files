import java.util.List;
import java.util.Arrays;

class Lam {
    public static void main(String[] args) {
        List<Integer> numbers=Arrays.asList(1,2,3,4,5,6,7,8,9);

        // традиционный цикл
        for(int i=0;i<numbers.size();i++) {
            System.out.print(numbers.get(i)+" ");
        }
        System.out.println();

        // цикл по коллекции
        for(int i: numbers) {
            System.out.print(i+" ");
        }
        System.out.println();

        // функциональный цикл
        numbers.forEach(val->System.out.print(val+" "));
        System.out.println();

        // сортировка
        Collections.sort(numbers, Collections.reverseOrder());
        numbers.forEach(val->System.out.print(val+" "));
        System.out.println();
    }
}