import java.util.*;

public class Main {

    public static void main(String[] args) {
        List<Integer> numbers=new ArrayList<>();
        numbers.addAll(
                Arrays.asList(1,2,3,4,5,6)
        );


        numbers.removeIf(val -> val>3);
        numbers.forEach(val->System.out.print(val+" "));
        System.out.println();

        numbers.replaceAll(val -> val*val);
        numbers.forEach(val->System.out.print(val+" "));
        System.out.println();

                
    }
}