import java.util.List;
import java.util.Arrays;
import java.util.*;

class Person {
    private String name;
    private int age;
    public Person(String n,int a) {
        name=n;
        age=a;
    }
    public int getAge() {
        return age;
    }
    public String getName() {
        return name;
    }
}

class Lam {
    public static void main(String[] args) {
        List<Person> people=Arrays.asList(new Person("Оля",34),
                                          new Person("Вася",31),
                                          new Person("Саша",45));

        // сортировка
        Collections.sort(people, (h1,h2)->h1.getName().compareTo(h2.getName()));
        people.forEach(h->System.out.print(h.getName()+" "));
        System.out.println();
    }
}