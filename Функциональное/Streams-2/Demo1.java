import java.util.stream.IntStream;
import java.util.*;


class Demo1 {
    public static void main(String[] args) {
        
        List<String> myList =
            Arrays.asList("a1", "a2", "b1", "c2", "c1");

        myList
          .stream()
          .filter(s -> s.startsWith("c"))
          .map(String::toUpperCase)
          .sorted()
          .forEach(System.out::println);

        myList
          .stream()
          .forEach(System.out::println);

        //List<Integer> myFoo = new ArrayList<>();
        IntStream.range(1,4)
          .map(n -> 2 * n + 1)
          .average()
          .ifPresent(System.out::println);  // 5.0
    }
}