import  java.util.*;
import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) {
        IntStream.range(1,1)
        .map(n -> 2 * n + 1)
        .average()
        .ifPresent(System.out::println);  // 5.0

    }
}
