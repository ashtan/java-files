import java.util.Arrays;
import java.util.*;

public class HelloWorld
{
    public static void main(String args[]) {
        Runnable fun = () -> System.out.println("Hello world!");
        fun.run();
        List<Integer> list=Arrays.asList(1,2,3,4,5,6,7,8,9);

        list.stream()
            .map(val -> val*val)
            .forEach(val -> System.out.println(val));


        List<Integer> list2=new ArrayList<Integer>();
        list.stream().filter(val -> val%2==0 && val>5).forEach(val->list2.add(val));
        System.out.println(list2);

        OptionalInt m=Arrays.stream(new int[] {1, 2, 3,444,2,4,5,3,343,45,45,4})
              .filter(val -> val%2==0)
              .max();

              //.ifPresent(System.out::println); 
        System.out.println(m);

    }
}