class A {
	private int value;
	public A(int v) {
		value=v;
	}
    public void set(int val) {
        value=val;
    }
    public int getInnerData() {
        B inner=new B(-10);
        return inner.getData();
    }
	class B {
		private int data;
		public B(int data) {
           this.data=data;
		}
		public int get() {
			return value;
		}
        public int getData() { 
            return data;
        }
	}
	public B getB(int data) {
		return new B(data);
	}

}
public class Program
{
	public static void main(String[] args) {
          A a = new A(1);
          A.B b = a.getB(10);
          a.set(-1);
          A.B c = a.new B(100);
		      A.B d = a.new B(1000);

        System.out.println(a.getInnerData());
        System.out.println(b.get());
        System.out.println(c.get());
		    System.out.println(b.getData());
        System.out.println(c.getData());
        System.out.println(d.getData());

	}
}
