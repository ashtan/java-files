class A {
    private static int value=1;
    private int nonstaticvalue=10;

	static class B {
        private int num=100;
		private static int data=2;

		public static int get() {
			return A.value;
		}
        public int getPro() {
            //return get();
            A a=new A();
            return a.nonstaticvalue;
        }
        public static void main(String[] args) {
            System.out.println("Hello from B");
        }
	}
	public static int get() {
		return B.data;
	}
    public int getBnum()
    {
        B b=new B();
        return b.num;
    }
}
public class Program
{
	public static void main(String[] args) {
        A a=new A();
        System.out.println(a.getBnum());

        //A.B.main(args);
        //A.B b=new A.B();
        //System.out.println(b.getPro());

          //System.out.println(A.get());
          //System.out.println(A.B.get());
	}
} 