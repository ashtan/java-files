interface Employee { 
    void info();
}

public class Programm {
    public static void main(String[] args) { 
        Employee emp=new Employee() {
            public void info() { 
                System.out.println("Yes!");
            } 
        };
        emp.info();

        new Employee () {
            public void info() { 
                System.out.println("Yes!");
            }
        }.info();

        new Object() {
            public void fun() {
                System.out.println(this.getClass().toString());
            }
        }.fun();
    }
}