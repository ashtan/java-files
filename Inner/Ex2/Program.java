interface Employee {
    void work();
    void pay();
}
public class Program {
    public static void main(String[] args) {
        Employee emp=new Employee() {
            public void work() {
                System.out.println("Я тружусь");
            }
          public void pay() {
                System.out.println("Я заработал 100000");
            }            
        };
        emp.work();
        emp.pay();

    }
}
