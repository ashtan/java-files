enum ParrotColors {
  RED,GREEN,BLUE
}

class Parrot
{
    private ParrotColors color;
    private String name;
    private int age;
    private static boolean canFly;

    static {
      System.out.println("Parrot");
    }

    public Parrot(String name) {
        this(name,0,ParrotColors.BLUE);
    }
    public Parrot(String name,int age,ParrotColors color) {
        this(name,age,color,true);
    }
    public Parrot(String name,int age, ParrotColors color,boolean canFly) {
      this.name=name;
      this.age=age;
      this.color=color;
      this.canFly=canFly;
    }
    public void setName(String name) {
      this.name=name;
    }
    public String getName() {
       return name;
    }
    public String getColor() {
      return color.toString();
    }
    public void say(String phrase) {
        System.out.println(name+":"+phrase);
    }
    public static void fly() {
        System.out.println(canFly?"Могу летать":"Не могу летать");

    }
    public Parrot getNewParrot(String name) {
        return new Parrot(name);
    }
}
public class ParrotDemo
{
    static {
      System.out.println("ParrotDemo");

    }
    public static void main(String[] args) {
      System.out.println("Привет!");
      Parrot kesha=new Parrot("Кеша",2,ParrotColors.RED);
      Parrot gosha=new Parrot("Гоша",6,ParrotColors.GREEN);
      System.out.println(kesha.getColor());
      System.out.println(gosha.getColor());
    }
}
