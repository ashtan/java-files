class Person {
	protected String name;
	protected int born;
	public Person(String name,int born) {
		this.name=name;
		this.born=born;
	}
	public void info() {
		System.out.print(name+"-"+born);
	}
}
class Employee extends Person {
	protected int payment;
	public Employee(String name,int born,int pay) {
		super(name,born);
		this.payment=pay;
	}
	@Override
	public void info() {
		super.info();
		System.out.println(":"+payment);
	}	
}
public class Program {
	public static void main(String[] args) {
        Employee emp=new Employee("Иванов А.А.",1988,35000);
        emp.info();
        Person per=emp;
        per.info();
	}
}