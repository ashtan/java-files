interface A {
    void fA();
} 
interface B {
    void fB();
}
interface C extends A, B {
    void fC();
}


abstract class Writer {
    public abstract void write();
}

class Pen extends Writer {
    public void write() {
        System.out.println("Ручка");
    }
}
class Pensil extends Writer {
    public void write() {
        System.out.println("Карандаш");
    }
}
class Marker extends Writer {
    public void write() {
        System.out.println("Фломастер");
    }
}

public class Program {
    public static void main(String[] args) {
        Writer[] arr={new Pen(),new Pensil(),new Marker(),new Pen(),new Pen()};

        for(int i=0;i<arr.length;i++) {
            arr[i].write();
        }
    }
}