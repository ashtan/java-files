abstract class Figure {
    abstract public void draw();
}

class Point extends Figure {
    protected int x,y;
    @Override
    public void draw() {
        System.out.println("("+x+","+y+")");
    }
    public Point (int x,int y) {
        this.x=x;
        this.y=y;
    }
}
public class Demo {
    public static void main(String []args) {
        Figure fig=new Point(10,20);
        //Figure fig2=new Figure();
        fig.draw();
    }
}