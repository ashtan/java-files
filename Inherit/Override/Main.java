class Z {
    public void get() { System.out.println("Z"); }
}

class A extends Z { 
     public void get() {
        super.get();
        System.out.println("A");
    }
}
class B extends A {
     public void get() {
        super.get();
        System.out.println("B");
    }
} 
class C extends B {
     public void get() {
        super.get();
        System.out.println("C");
    }
} 

class Main {
   public static void main(String[] args) {
     A one=new A();
     A two=new B();
     A three=new C();


     one.get();
     System.out.println("-----------");
     two.get();
     System.out.println("-----------");

     three.get();
   } 
}
