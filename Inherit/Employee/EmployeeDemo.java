class Person {
   protected String name;
   protected int born;
   public Person(String name,int born) {
     this.name=name;
     this.born=born; 
 }
  public void info() { 
    System.out.println(name+"-"+born);
 } 
}
class Client extends Person {
   protected int cash;
   public Client(String name,int born,int cash) {
     super(name,born);
     this.cash=cash; 
   }
  @Override
  public void info() { 
    super.info();
    System.out.println("Cash:"+cash);
 }   
}

class Employee extends Person {
   protected int payment;
   public Employee(String name,int born,int pay) {
     super(name,born);
     this.payment=pay; 
   }
  @Override
  public void info() { 
    super.info();
    System.out.println("Payment:"+payment);
 }   
}

public class EmployeeDemo
{
    public static void main(String[] args) {
     Person[] persons = new Person[2];
     persons[0]=new Employee("Иванов А.А.",1988,35000);
     persons[1]=new Client("Петров В.В.",1968,100);

     for(Person p: persons) {
        p.info();
     }
 }
}