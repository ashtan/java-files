public class Program {
    public static void main(String [] args) {
        Employee[] staff=new Employee[3];
        staff[0]=new Driver(1,"Иванов",100);
        staff[1]=new Cleaner(2,"Сидорова",50);
        staff[2]=new Cleaner(3,"Петрова",50);

        for(int i=0;i<staff.length;i++)
        {
            staff[i].work(8);
            staff[i].calc();
            staff[i].info();
        }
    }
}