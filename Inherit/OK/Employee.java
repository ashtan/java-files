interface HourWork {
    void calcWorkHours();
}

class Employee {
    protected String name;
    protected int id;
    protected int pay;
    protected int worktime;
    public Employee(int id, String name) {
        this.id=id;
        this.name=name;
        worktime=0;
    }
    public void calc() {
         pay=0;
    }
    public void work(int hours) {
         worktime=hours;
    }
    public void info() {
        System.out.println("ID: "+id+" Имя: "+name+ 
            " Должность: "+this.getClass().getName()+
            " Отработано: "+worktime+" часов "+" Зарплата: "+pay);
    }
}

abstract class Personal extends Employee implements HourWork {
   protected int hourPay;
   public Personal(int id, String name,int hourPay) {
      super(id,name);
      worktime=0;
      this.hourPay=hourPay;
   }
   public void work(int hours) {
      worktime=hours;      
   }
    public void calcWorkHours() {
      pay=hourPay*worktime;
   }  
}

class Cleaner extends Personal {
   public Cleaner(int id, String name,int hourPay) {
      super(id,name,hourPay);
   }
   public void calc() {
      calcWorkHours();
   }
}


class Driver extends Personal {
   public Driver(int id, String name,int hourPay) {
      super(id,name,hourPay);
   }

   public void calc() {
      calcWorkHours();
      pay+=500;
   }
}
