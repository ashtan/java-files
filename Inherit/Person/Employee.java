class Employee extends Person {
   public int payment;
   public Employee(String name,int born,int pay) {
     super(name,born);
     this.payment=pay; 
   }
   @Override
   public void info() {
     super.info();
     System.out.println("-"+payment);
   }
   int getPayment() {
      return payment;
   }
}