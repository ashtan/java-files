interface Callable {
    void fun();
}
class A {
    public void fun() {
        System.out.println("A");
    }
}
class B extends A implements Callable {
    //public void fun() {
    //    System.out.println("B");
    //}
}
public class Main {
    public static void main(String[] args) {
        B b=new B();
        b.fun();
        Callable c=new B();
        
    }
}