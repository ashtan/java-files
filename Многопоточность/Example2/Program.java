class MyThread extends Thread {
    public void run() {
        try {
            Thread.sleep(5000);
          }
        catch(InterruptedException ex) { }
        System.out.println("Привет из побочного потока!");
    }
}
public class Program  {
    static MyThread t;
    
    public static void main(String[] args) {
        t = new MyThread(); //Создание потока
        t.start(); //Запуск потока
        
        try {
           Thread.sleep(5000);
           //t.join();
        }
        catch(InterruptedException ex) { }

        
        System.out.println("Главный поток завершён...");
    }
}