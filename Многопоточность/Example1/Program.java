class SomeThing implements Runnable  {
    //Этот метод будет выполняться в побочном потоке
    public void run() {
        for(int i=0;i<100;i++) {
         try {
           Thread.sleep(10);
         }
         catch(InterruptedException ex) {}

         System.out.println("Привет из побочного потока!");
        }
        System.out.println("Побочный поток завершён...");
    }
}
public class Program  {
    public static void main(String[] args)
    {
        Thread t = new Thread(new SomeThing());
        t.start();
        //try {
        //  t.join();
        //}
        //catch(InterruptedException ex) {}

        for(int i=0;i<100;i++) {
           try {
             Thread.sleep(10);
           }
           catch(InterruptedException ex) {}
           System.out.println("Привет из главного потока!");
        }
        System.out.println("Главный поток завершён...");
    }
}