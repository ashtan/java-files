public class HelloThread extends Thread {
    public HelloThread() {
        setDaemon(true);
    }
    public void run() {
        try {
         for(int i=0;i<10;i++) {
           Thread.sleep(1000);
           System.out.print("Привет из побочного потока!"+
            this.getId()+"\n");
          }
         } catch(InterruptedException ex) {}
    }
    public static void main(String args[]) {
        (new HelloThread()).start();
        (new HelloThread()).start();
        (new HelloThread()).start();
        (new HelloThread()).start();
        (new HelloThread()).start();
        (new HelloThread()).start();
        (new HelloThread()).start();
        (new HelloThread()).start();
        (new HelloThread()).start();
        (new HelloThread()).start();
        (new HelloThread()).start();
        (new HelloThread()).start();
        (new HelloThread()).start();

         try {
         
           Thread.sleep(3000);
          
         } catch(InterruptedException ex) {}
        System.out.println("Привет из главного потока!");

    }
}