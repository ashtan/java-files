import static org.junit.Assert.*;

public class CalculatorTest {

    @org.junit.Test
    public void add() {
        assertEquals(8,Calculator.add(5,3));
    }

    @org.junit.Test
    public void sub() {
        assertEquals(2,Calculator.sub(5,3));

    }

    @org.junit.Test
    public void mul() {
        assertEquals(15,Calculator.mul(5,3));

    }

    @org.junit.Test
    public void div() {
        assertEquals(1,Calculator.div(5,3));

    }
    @org.junit.Test(expected = ArithmeticException.class)
    public void div0() {
        Calculator.div(5,0);
    }
    @org.junit.Test(timeout=100)
    public void proc() {
        int sum=0;
        for(int i=0;i<100000000;i++)
            sum+=Calculator.div(sum,12345);
    }
}