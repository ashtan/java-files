public class Main {

    public static void fun(Temp x) {
        x.a++;
    }
    public static void main(String[] args) {
        Temp t1=new Temp();
        Temp t2=new Temp();
        Temp t3=t1;
        t1.a=10;
        t2.a=10;
        System.out.println(t1==t3);
        System.out.println(t1.equals(t2));

        Person p1=new Person();
        Person p2=new Person();
        p1.t=t1;
        p2.t=t2;
        System.out.println(p1.equals(p2));

    }
}



class Person {
    int age;
    Temp t;

    public boolean equals(Person p) {
        return (age==p.age) && (t==p.t);
    }
}
class Temp
{
    int a;
    public boolean equals(Temp x) {
        return a==x.a;
    }
}
